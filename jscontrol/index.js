const util = require('util');
const noble = require('@abandonware/noble');
const debug = require('debug')('jscontrol');

const LightMugService = '6e400001b5a3f393e0a9e50e24dcca9e';
const txUuid = '6e400002b5a3f393e0a9e50e24dcca9e';
const rxUuid = '6e400003b5a3f393e0a9e50e24dcca9e';
const bluefruitCommandPrefix = Buffer.from('!C', 'ascii');
const blink1CommandPrefix = Buffer.from('0163', 'hex');
const DECIMAL = 10;
let peripheral;

noble.on('stateChange', function(state) {
  if (state === 'poweredOn') {
    console.log('startScanning');
    noble.startScanning();
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', p => {
  const { advertisement, id, address, addressType } = p;
  const {
    localName,
    txPowerLevel,
    manufacturerData,
    serviceData,
    serviceUuids,
  } = advertisement;

  if (
    localName == 'LightMug' ||
    (serviceUuids && serviceUuids.includes(LightMugService))
  ) {
    peripheral = p;
    noble.stopScanning();
    // console.log("peripheral", {id, address, addressType});
    explore(p);
  }
});

function explore(peripheral) {
  peripheral.on('disconnect', () => {
    console.log('disconnect');
    process.exit(0);
  });

  peripheral.connect(async error => {
    if (error) {
      console.log(error);
      return;
    }
    console.log('Connected');
    const discoverServices = util.promisify(
      peripheral.discoverServices.bind(peripheral),
    );
    const services = await discoverServices([]);

    await Promise.all(services.map(discoverService));
    peripheral.disconnect();
  });
}

async function discoverService(service) {
  const { uuid, name, type } = service;
  // console.log("service", { uuid, name, type });
  const discoverCharacteristics = util.promisify(
    service.discoverCharacteristics.bind(service),
  );
  const characteristics = await discoverCharacteristics([]);
  let tx, rx;

  await Promise.all(
    characteristics.map(async characteristic => {
      const { uuid, properties, name, type } = characteristic;

      if (service.uuid === LightMugService) {
        switch (uuid) {
          case txUuid:
            tx = characteristic;
            break;
          case rxUuid:
            rx = characteristic;
            characteristic.on('data', rxCallback);
            characteristic.subscribe(error => error && console.log(error));
            break;
        }
      } else if (service.type === 'org.bluetooth.service.battery_service') {
        switch (type) {
          case 'org.bluetooth.characteristic.battery_level':
            characteristic.read();
            const read = util.promisify(
              characteristic.read.bind(characteristic),
            );
            const data = await read();
            console.log(`Battery: ${data[0]}%`);
            break;
        }
      }
    }),
  );

  if (tx && rx) {
    if (process.argv.length < 3) {
      console.log('No color provided, exiting');
      peripheral.disconnect();
      return;
    }
    const color = Buffer.from(process.argv[2], 'hex');
    let time = 0;
    if (process.argv.length == 4) {
      time = parseInt(process.argv[3], DECIMAL) / 10;
    }
    const timing = Buffer.alloc(2);
    timing.writeUInt16BE(time);
    const command = Buffer.concat([blink1CommandPrefix, color, timing]);
    console.log(`Sending ${command.toString('hex')}`);
    tx.write(command, tx.properties.includes('writeWithoutResponse'), error => {
      if (error) {
        console.log(error);
        return;
      }
    });
  }
}

function rxCallback(data) {
  console.log('rx characteristic', data.toString('hex'));
}
