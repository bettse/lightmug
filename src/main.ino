/*********************************************************************
  This is an example for our nRF51822 based Bluefruit LE modules

  Pick one up today in the adafruit shop!

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  MIT license, check LICENSE for more information
  All text above, and the splash screen below must be included in
  any redistribution
 *********************************************************************/

#include <math.h>
#include <string.h>
#include <Arduino.h>
#include <SPI.h>
#if not defined(_VARIANT_ARDUINO_DUE_X_) && not defined(_VARIANT_ARDUINO_ZERO_)
#include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BLEBattery.h"

#include "BluefruitConfig.h"

#include <NeoPixelBus.h>
#include <NeoPixelAnimator.h>
#include <NeoPixelAnimator.h>
#include <Battery.h>


/*=========================================================================
  APPLICATION SETTINGS

      FACTORYRESET_ENABLE       Perform a factory reset when running this sketch
     
                                Enabling this will put your Bluefruit LE module
  in a 'known good' state and clear any config
  data set in previous sketches or projects, so
                                running this at least once is a good idea.
     
                                When deploying your project, however, you will
  want to disable factory reset by setting this
  value to 0.  If you are making changes to your
                                Bluefruit LE device via AT commands, and those
  changes aren't persisting across resets, this
  is the reason why.  Factory reset will erase
  the non-volatile memory where config data is
  stored, setting it back to factory default
  values.
         
                                Some sketches that require you to bond to a
  central device (HID mouse, keyboard, etc.)
  won't work at all with this feature enabled
  since the factory reset will clear all of the
  bonding data stored on the chip, meaning the
  central device won't be able to reconnect.
  PIN                       Which pin on the Arduino is connected to the NeoPixels?
  NUMPIXELS                 How many NeoPixels are attached to the Arduino?
  -----------------------------------------------------------------------*/
#define FACTORYRESET_ENABLE 0

#define VBATPIN A9
#define PIN 6
#define NUMPIXELS 12
#define BLINK1_REPORT_ID  1
#define BLINK1_REPORT_SIZE 8
#define BLINK1_BUF_SIZE (BLINK1_REPORT_SIZE+1)
#define COLOR_PATTERNS_SIZE 12

#define BTN_UP 5
#define BTN_DOWN 6
#define BTN_LEFT 7
#define BTN_RIGHT 8
/*=========================================================================*/

struct RGB {
  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;
  uint8_t th = 0;
  uint8_t tl = 0;
  uint8_t n = 0;
};

struct Tickle {
  uint8_t on = 0;
  uint8_t th = 0;
  uint8_t tl = 0;
  uint8_t st = 0;
  uint8_t sp = 0;
  uint8_t ep = COLOR_PATTERNS_SIZE - 1;
};

struct PlayLoop {
  uint8_t on = 0;
  uint8_t sp = 0;
  uint8_t ep = COLOR_PATTERNS_SIZE - 1;
  uint8_t c = 0;
  uint8_t noop1 = 0;
  uint8_t noop2 = 0;
};

struct Button {
  uint8_t number = 0;
  uint8_t pressed = 0;
};

struct Packet {
  uint8_t report_id = 0;
  char command = ' ';
  union Paramaters {
    struct RGB rgb;
    struct Tickle tickle;
    struct PlayLoop playloop;
    struct Button button;
  } parameters;
};

union PacketBuffer {
  struct Packet packet;
  uint8_t buffer[sizeof(struct Packet)];
};

struct MyAnimationState {
  RgbwColor StartingColor;  // the color the animation starts at
  RgbwColor EndingColor; // the color the animation will end at
  AnimEaseFunction Easeing; // the acceleration curve it will use 
};

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);
Adafruit_BLEBattery battery(ble);

const uint16_t PixelCount = NUMPIXELS;
NeoPixelBus<NeoGrbwFeature, Neo800KbpsMethod> strip(NUMPIXELS, PIN);
NeoPixelAnimator animations(PixelCount, NEO_MILLISECONDS);
MyAnimationState animationState[PixelCount];

int32_t version;

RGB colorPatterns[COLOR_PATTERNS_SIZE];
PlayLoop playing;
uint8_t playindex = 0;
Tickle tickle;
uint8_t currentBattery = 0;

// A small helper
void error(const __FlashStringHelper *err) {
  Serial.println(err);
  while (1);
}

void setup(void) {
  Serial.begin(115200);

  for (uint16_t pixel = 0; pixel < PixelCount; pixel++) {
    strip.SetPixelColor(pixel, RgbwColor(0, 0, 0, 0));
  }
  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));

  if (!ble.begin(VERBOSE_MODE)) {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println(F("OK!"));

  if (FACTORYRESET_ENABLE) {
    /* Perform a factory reset to make sure everything is in a known state */
    Serial.println(F("Performing a factory reset: "));
    if (!ble.factoryReset()) {
      error(F("Couldn't factory reset"));
    }
  }

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();
  battery.begin(true);
  if (! ble.sendCommandCheckOK(F("AT+GAPDEVNAME=LightMug")) ) {
    error(F("Could not set device name"));
  }

  if (! ble.sendCommandCheckOK(F("AT+HWMODELED=DISABLE")) ) {
    error(F("Could not disable HW MODE LED"));
  }

  if (! ble.sendCommandWithIntReply(F("ATI=4"), &version) ) {
    error(F("Could not get version?"));
  }

  ble.verbose(false);
  ble.setMode(BLUEFRUIT_MODE_DATA);
  Serial.println("Setup complete");
}

void loop(void) {
  size_t len = 0;
  RgbwColor color;
  PacketBuffer pb = {};

  updateBatteryPercentage();

  if (ble.isConnected()) {
    while (ble.available() && len < BLINK1_REPORT_SIZE) {
      uint8_t c = ble.read();
      pb.buffer[len++] = c;
    }
  }
  if (len > 0) {
    Serial.print("New command: ");
    printHex(pb.buffer, len);
  }

  Packet packet = pb.packet;
  if (packet.report_id == 1) {
    switch (packet.command) {
      case 'c':
      case 'n':
        fadeToRGB(packet.parameters.rgb);
        break;
      case 'r':
        readRGB(packet);
        break;
      case 'D':
        setTickle(packet);
        break;
      case 'p':
        setPlayLoop(packet);
        break;
      case 'S':
        getPlayState(packet);
        break;
      case 'P':
        setColorPatternLine(packet);
        break;
      case 'R':
        getColorPatternLine(packet);
        break;
      case 'W':
        savePattern(packet);
        break;
      case 'v':
        getVersion(packet);
        break;
      case '!':
        test();
        break;
    }
  } else if (packet.report_id == '!') {
    // Adafruit Bluefruit iOS app commands
    bluefruit(packet);
  }

  animations.UpdateAnimations();
  strip.Show();
}

void setTickle(Packet packet) {
  Serial.println("Set Tickle");
  tickle = packet.parameters.tickle;
}

void setPlayLoop(Packet packet) {
  Serial.println("Set Play Loop");
  if (packet.parameters.playloop.ep > COLOR_PATTERNS_SIZE - 1) {
    packet.parameters.playloop.ep = COLOR_PATTERNS_SIZE - 1;
  }
  playing = packet.parameters.playloop;
}

void getPlayState(Packet packet) {
  Serial.println("Get Play State");
  uint8_t reply[BLINK1_BUF_SIZE] = {
    0x01,
    'S',
    playing.on,
    playing.sp,
    playing.ep,
    playing.c,
    playindex,
    0,
    '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void getColorPatternLine(Packet packet) {
  Serial.println("Get Color Pattern Line");
  RGB rgb = packet.parameters.rgb;
  RGB pattern = colorPatterns[rgb.n];
  uint8_t reply[BLINK1_BUF_SIZE] = {
    0x01,
    'R',
    pattern.r,
    pattern.b,
    pattern.b,
    pattern.th,
    pattern.tl,
    rgb.n,
    '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void setColorPatternLine(Packet packet) {
  Serial.println("Set Color Pattern Line");
  RGB rgb = packet.parameters.rgb;
  if (rgb.n < COLOR_PATTERNS_SIZE) {
    colorPatterns[rgb.n] = rgb;
  }
}

void savePattern(Packet packet) {
  Serial.println("Save Pattern");
  PacketBuffer pb = {};
  pb.packet = packet;
  if (pb.buffer[2] == 0xBE && pb.buffer[3] == 0xEF && pb.buffer[4] == 0xCA && pb.buffer[6] == 0xFE) {
  }
}

void getVersion(Packet packet) {
  Serial.println("Get Version");
  char reply[BLINK1_BUF_SIZE] = {0x01, packet.command, 0, 'B', '1', 0, 0, 0, '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}


void test() {
  Serial.println("Test command");
}

// feather_bluefruit_neopixel_animation_controller.ino
void bluefruit(Packet packet) {
  Serial.println("Bluefruit command");
  RgbwColor last;

  // Color
  if (packet.command == 'C') {
    packet.parameters.rgb.n = 0; //All
    packet.parameters.rgb.th = 0; //Instant
    packet.parameters.rgb.tl = 0; //Instant
    fadeToRGB(packet.parameters.rgb);
  }

  // Buttons
  if (packet.command == 'B') {
    uint8_t buttnum = packet.parameters.button.number - '0';
    boolean pressed = packet.parameters.button.pressed - '0';
    Serial.print("Button ");
    Serial.print(buttnum);
    if (pressed) {
      Serial.println(" pressed");
    }  else {
      Serial.println(" released");
      return; // Ignore releases until I refactor later
    }

    switch (buttnum) {
      case 1:
        rainbow(20);
        break;
      case 2:
        colorWipe(RgbColor(114, 0, 255), 20);
        colorWipe(RgbColor(0, 0, 0), 20);
        colorWipe(RgbColor(0, 50, 255), 20);
        colorWipe(RgbColor(0, 0, 0), 20);
        colorWipe(RgbColor(0, 220, 255), 20);
        colorWipe(RgbColor(0, 0, 0), 20);
        colorWipe(RgbColor(255, 225, 255), 20);
        colorWipe(RgbColor(0, 0, 0), 20);
        break;
      case 3:
        for (uint16_t i = 0; i < PixelCount; i++) {
          strip.SetPixelColor(i, RgbColor(0, 0, 0));
        }
        colorWipe(RgbColor(0, 0, 255), 20);
        colorWipe(RgbColor(0, 0, 0), 20);
        break;
      case 4:
        for (uint16_t i = 0; i < PixelCount; i++) {
          strip.SetPixelColor(i, RgbColor(0, 0, 0));
        }
        rainbowCycle(10);
        break;
      case BTN_UP:
        Serial.println("Lighten");
        for (uint16_t i = 0; i < PixelCount; i++) {
          RgbwColor color = strip.GetPixelColor(i);
          color.Lighten(10);
          strip.SetPixelColor(i, color);
        }
        strip.Show(); // This sends the updated RgbColor to the hardware.
        break;
      case BTN_DOWN:
        Serial.println("Darken");
        for (uint16_t i = 0; i < PixelCount; i++) {
          RgbwColor color = strip.GetPixelColor(i);
          color.Darken(10);
          strip.SetPixelColor(i, color);
        }
        strip.Show(); // This sends the updated RgbColor to the hardware.
        break;
      case BTN_RIGHT: // Rotate clockwise
        last = strip.GetPixelColor(PixelCount - 1);
        for (uint16_t i = 0; i < PixelCount; i++) {
          RgbwColor color = strip.GetPixelColor(i);
          strip.SetPixelColor(i, last);
          last = color;
        }
        break;
      case BTN_LEFT: // Rotate counter-clockwise
        last = strip.GetPixelColor(0);
        for (uint16_t i = PixelCount; i > 0; i--) {
          RgbwColor color = strip.GetPixelColor(i - 1);
          strip.SetPixelColor(i - 1, last);
          last = color;
        }
        break;
      default:
        Serial.print("Unhandled buttnum: ");
        Serial.println(buttnum);
    }
  }
}

// Fill the dots one after the other with a color
void colorWipe(RgbColor c, uint8_t wait) {
  for (uint16_t i = 0; i < PixelCount; i++) {
    strip.SetPixelColor(i, c);
    strip.Show();
    delay(wait);
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for (j = 0; j < 256; j++) {
    for (i = 0; i < PixelCount; i++) {
      strip.SetPixelColor(i, Wheel((i + j) & 255));
    }
    strip.Show();
    delay(wait);
  }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for (j = 0; j < 256 * 5; j++) { // 5 cycles of all colors on wheel
    for (i = 0; i < PixelCount; i++) {
      strip.SetPixelColor(i, Wheel(((i * 256 / PixelCount) + j) & 255));
    }
    strip.Show();
    delay(wait);
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
RgbColor Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if (WheelPos < 85) {
    return RgbColor(255 - WheelPos * 3, 0, WheelPos * 3);
  }
  if (WheelPos < 170) {
    WheelPos -= 85;
    return RgbColor(0, WheelPos * 3, 255 - WheelPos * 3);
  }
  WheelPos -= 170;
  return RgbColor(WheelPos * 3, 255 - WheelPos * 3, 0);
}

void fadeToRGB(RGB rgb) {
  Serial.println("Fade to RGB");
  int time = (rgb.th << 8 | rgb.tl) * 10;
  RgbwColor c = RgbwColor(rgb.r, rgb.g, rgb.b);

  switch (rgb.n) {
    case 0: //All
      for (uint16_t pixel = 0; pixel < PixelCount; pixel++) {
        fadePixel(pixel, c, time);
      }
      break;
    case 1: //Odds
      for (uint16_t pixel = 1; pixel < PixelCount; pixel = pixel + 2) {
        fadePixel(pixel, c, time);
      }
      break;
    case 2: //Evens
      for (uint16_t pixel = 0; pixel < PixelCount; pixel = pixel + 2) {
        fadePixel(pixel, c, time);
      }
      break;
    default:
      fadePixel(rgb.n, c, time);
  }
}

void fadePixel(uint8_t n, RgbwColor c, uint16_t time) {
  animationState[n].StartingColor = strip.GetPixelColor(n);
  animationState[n].EndingColor = c;
  animationState[n].Easeing = NeoEase::Linear;
  animations.StartAnimation(n, time, AnimUpdate);
}

void readRGB(Packet packet) {
  Serial.println("Read RGB");
  RGB rgb = packet.parameters.rgb;
  RgbwColor color = strip.GetPixelColor(rgb.n);
  uint8_t reply[BLINK1_BUF_SIZE] = {0x01, 'r', color.R, color.G, color.B, 0, 0, rgb.n, '\n'};
  ble.write(reply, BLINK1_BUF_SIZE);
}

void AnimUpdate(const AnimationParam& param) {
  float progress = animationState[param.index].Easeing(param.progress);

  RgbwColor updatedColor = RgbwColor::LinearBlend(
      animationState[param.index].StartingColor,
      animationState[param.index].EndingColor,
      progress);
  strip.SetPixelColor(param.index, updatedColor);

  if (param.state == AnimationState_Completed) {
    if (playing.on) {
      RGB next = colorPatterns[playindex++];
      fadeToRGB(next);

      //When the loop ends
      if (playindex >= playing.ep) {
        Serial.println("Start next loop");
        playindex = playing.sp;
        playing.c--;
      }

      if (playing.c <= 0) {
        Serial.println("Stop playing");
        playing.on = 0;
      }
    }
  }
}

void updateBatteryPercentage() {
  uint8_t newPercent = sigmoidal(analogRead(VBATPIN) * 2, 3500, 4200);
  if (newPercent != currentBattery) {
    currentBattery = newPercent;
    battery.update(currentBattery);
  }
}

void printHex(const uint8_t *data, const uint32_t numBytes) {
  uint32_t szPos;
  for (szPos = 0; szPos < numBytes; szPos++) {
    Serial.print(F("0x"));
    // Append leading 0 for small values
    if (data[szPos] <= 0xF) {
      Serial.print(F("0"));
      Serial.print(data[szPos] & 0xf, HEX);
    } else {
      Serial.print(data[szPos] & 0xff, HEX);
    }
    // Add a trailing space if appropriate
    if ((numBytes > 1) && (szPos != numBytes - 1)) {
      Serial.print(F(" "));
    }
  }
  Serial.println();
}
